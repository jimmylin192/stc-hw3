#define SIZE 2048
#include "stdio.h"
#include "time.h"
#include "mkl.h"

// #define TESTING

int main () {
    time_t lu_time, sol_time;
    //##################################################################
    // 2a. construct a diagonally dominant matrix
    //##################################################################
    int i, j, k = 0;
    double* A = (double*) mkl_malloc ( SIZE*SIZE*sizeof(double), 64 );
    int CAP = SIZE * SIZE;
    for (i = 0; i < CAP; i += SIZE+1) A[i] = 100.0; // diagonal 
    for (i = 1; i < CAP; i += SIZE+1) A[i] = 1.0; // upper diagonal
    for (i = SIZE; i < CAP; i += SIZE+1) A[i] = 1.0; // lower diagonal
#ifdef TESTING
    printf("initialized A: \n");
    for (i = 0; i < SIZE; i ++) {
        for (j = 0; j < SIZE; j ++) 
            printf ("%f ", A[i*SIZE+j]);
        printf("\n");
    }
#endif
    //##################################################################
    // 2b. create a random solution vector x and 
    // generate the right hand side vector b by the blas-2 routine
    //##################################################################
    double* x = (double*) mkl_malloc ( SIZE*sizeof(double), 64 );
    double* b = (double*) mkl_malloc ( SIZE*sizeof(double), 64 );
    srand(0);
    for (i = 0; i < SIZE; i ++) x[i] = (rand() % 10) - 5.0;
    cblas_dgemv(CblasRowMajor, CblasNoTrans,   // layout, op(a)
                SIZE, SIZE,                    // m, n
                1.0, A, SIZE,                  // alpha, a, lda
                x, 1,                          // x, incx
                0.0, b, 1                      // beta, y, incy
               );
#ifdef TESTING
    printf("randomized x: \n");
    for (j = 0; j < SIZE; j ++) printf ("%f ", x[j]);
    printf("\n");
    printf("computed b: \n");
    for (j = 0; j < SIZE; j ++) printf ("%f ", b[j]);
    printf("\n");
#endif
    //##################################################################
    // 2c. Use LAPACK routine dgetrf() to factor A into L and U
    //##################################################################
    lapack_int* ipiv = (lapack_int*) mkl_malloc ( SIZE*sizeof(lapack_int), 64 );
    time_t dgetrf_prev = clock();
    LAPACKE_dgetrf(LAPACK_ROW_MAJOR,    // layout
                   SIZE, SIZE,          // m, n
                   A, SIZE, ipiv        // a, lda, ipiv
                  );
    lu_time = clock() - dgetrf_prev;
#ifdef TESTING
    printf("Factorized L and U on A: \n");
    for (i = 0; i < SIZE; i ++) {
        for (j = 0; j < SIZE; j ++) 
            printf ("%f ", A[i*SIZE+j]);
        printf("\n");
    }
#endif
    //##################################################################
    // 2d. Use LAPACK routine dgetrs() to solve the linear system
    //##################################################################
    time_t dgetrs_prev = clock();
    LAPACKE_dgetrs(LAPACK_ROW_MAJOR, 'N',      // layout, trans
                   SIZE, 1, A, SIZE,           // n, nrhs, a, lda
                   ipiv, b, 1                  // ipiv, b, ldb
                  );
    sol_time = clock() - dgetrs_prev;
#ifdef TESTING
    printf("solved x: \n");
    for (j = 0; j < SIZE; j ++) printf ("%f ", b[j]);
    printf("\n");
#endif
    printf ("Time Spent on computing LU Factorization: %d.\n", lu_time);
    printf ("Time Spent on computing solutions of Linear Equations: %d.\n", sol_time);
    printf ("Total Time Spent: %d.\n", lu_time+sol_time);
    //##################################################################
    // epilogue: reclaim variables
    //##################################################################
    mkl_free(ipiv);
    mkl_free(b);
    mkl_free(x);
    mkl_free(A);
    return 0;
}
