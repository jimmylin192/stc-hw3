#define SIZE 2000
#include "stdio.h"
#include "time.h"
#include "mkl.h"

// #define TESTING

int main () {
    //##################################################################
    // 3a. construct a diagonally dominant matrix
    //##################################################################
    int i, j, k = 0;
    double* A = (double*) mkl_malloc ( SIZE*SIZE*sizeof(double), 64 );
    int CAP = SIZE * SIZE;
    for (i = 0; i < CAP; i += SIZE+1) A[i] = 100.0; // diagonal 
    for (i = 1; i < CAP; i += SIZE+1) A[i] = 1.0; // upper diagonal
    for (i = SIZE; i < CAP; i += SIZE+1) A[i] = 1.0; // lower diagonal
#ifdef TESTING
    printf("initialized A: \n");
    for (i = 0; i < SIZE; i ++) {
        for (j = 0; j < SIZE; j ++) 
            printf ("%f ", A[i*SIZE+j]);
        printf("\n");
    }
#endif

    //##################################################################
    // 3a. create a random solution vector x and 
    // generate the right hand side vector b by the blas-2 routine
    //##################################################################
    double* x = (double*) mkl_malloc ( SIZE*sizeof(double), 64 );
    double* b = (double*) mkl_malloc ( SIZE*sizeof(double), 64 );
    srand(0);
    for (i = 0; i < SIZE; i ++) x[i] = (rand() % 10) - 5.0;
    cblas_dgemv(CblasRowMajor, CblasNoTrans,   // layout, op(a)
                SIZE, SIZE,                    // m, n
                1.0, A, SIZE,                  // alpha, a, lda
                x, 1,                          // x, incx
                0.0, b, 1                      // beta, y, incy
               );
#ifdef TESTING
    printf("randomized x: \n");
    for (j = 0; j < SIZE; j ++) printf ("%f ", x[j]);
    printf("\n");
    printf("computed b: \n");
    for (j = 0; j < SIZE; j ++) printf ("%f ", b[j]);
    printf("\n");
#endif

    //##################################################################
    // 3b. Use a vector consisting of all 1s as an inital guess
    //##################################################################
    double* z = (double*) mkl_malloc ( SIZE*sizeof(double), 64 );
    for (i = 0; i < SIZE; i ++) z[i] = 1.0;

    //##################################################################
    // 3c. Jacobi Iterations until 1e-5 relative error
    //##################################################################
    double* temp = (double*) mkl_malloc ( SIZE*sizeof(double), 64 );
    double* D = (double*) mkl_malloc ( SIZE*SIZE*sizeof(double), 64 );
    for (i = 0; i < CAP; i += SIZE+1) {
        D[i] = 1.0 / A[i]; 
        A[i] = 0.0;
    }

    double dist = 0.0, real_sol_norm = 0.0, rel_err = 1.0;
    real_sol_norm = cblas_dnrm2(SIZE, x, 1);
    int iter = 0;
    time_t start = clock();
    while (1) {
        // temp = b
        cblas_dcopy(SIZE, b, 1, temp, 1);
        // temp = temp - R z^{(k)}
        cblas_dgemv(CblasRowMajor, CblasNoTrans,  // layout, op(a)
                SIZE, SIZE,                       // m, n
                -1.0, A, SIZE,                    // alpha, a, lda
                z, 1,                             // x, incx
                1.0, temp, 1                      // beta, y, incy
               );
        // z^{(k+1)} = D^{-1} * temp
        cblas_dgemv(CblasRowMajor, CblasNoTrans,  // layout, op(a)
                SIZE, SIZE,                       // m, n
                1.0, D, SIZE,                     // alpha, a, lda
                temp, 1,                          // x, incx
                0.0, z, 1                         // beta, y, incy
               );
        // compute relative error and stopping condition
        vdSub(SIZE, z, x, temp);
        dist = cblas_dnrm2(SIZE, temp, 1);
        rel_err = dist / real_sol_norm;
        printf("iter=%d, dist2target=%f, relative_error=%f.\n", iter, dist, rel_err);
        if (rel_err < 1e-5) break;
        iter ++;
    }
    time_t converge_time = clock() - start;

#ifdef TESTING
    printf("solved x: \n");
    for (j = 0; j < SIZE; j ++) printf ("%f ", b[j]);
    printf("\n");
#endif

    //##################################################################
    // 3d. Measure the time it takes to converge
    //##################################################################
    printf ("Time taken to converge: %d.\n", converge_time);

    //##################################################################
    // epilogue: reclaim variables
    //##################################################################
    mkl_free(temp);
    mkl_free(z);
    mkl_free(b);
    mkl_free(x);
    mkl_free(A);
    return 0;
}
