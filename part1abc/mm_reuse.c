#define SIZE 2048
int main () {
    double A [SIZE][SIZE], B [SIZE][SIZE], C [SIZE][SIZE];
    int i, j, k;
    for (i = 0; i < SIZE; i ++)
        for (j = 0; j < SIZE; j ++) {
            A[i][j] = 1.0;
            B[i][j] = 1.0;
        }
    for (i = 0; i < SIZE; i ++)
        for (j = 0; j < SIZE; j ++) {
            int sum = 0;
            for (k = 0; k < SIZE; k ++) 
                sum = sum + A[i][k] * B[k][j];
            C[i][j] = sum;
        }
    return 0;
}
