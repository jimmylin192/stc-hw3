#define SIZE 2000
#include "mkl.h"

int main () {
    double *A = (double*) mkl_malloc ( SIZE*SIZE*sizeof(double), 64);
    double *B = (double*) mkl_malloc ( SIZE*SIZE*sizeof(double), 64);
    double *C = (double*) mkl_malloc ( SIZE*SIZE*sizeof(double), 64);
    int i, j, k = 0;
    for (i = 0; i < SIZE; i ++)
        for (j = 0; j < SIZE; j ++) {
            A[k] = 1.0;
            B[k] = 1.0;
            k ++;
        }
    cblas_dgemm ( 
            CblasRowMajor,       // 
            CblasNoTrans,        // op(A)
            CblasNoTrans,        // op(B)
            SIZE, SIZE, SIZE,    // m, n, k
            1.0, A, SIZE,        // alpha, A, lda
            B, SIZE,             //        B, ldb
            0.0, C, SIZE         // beta,  C, ldc
            );   
    // printf("%f\n", C[0]);
    return 0;
}
